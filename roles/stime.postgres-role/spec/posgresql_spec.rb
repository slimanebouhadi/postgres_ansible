require 'serverspec'

set :backend, :exec

#tomcat_version = '8.0.37'

describe group('exafr') do
    it { is_expected.to exist }
end

describe user('exafr1') do
    it { is_expected.to exist }
    it { is_expected.to belong_to_group 'exafr'}
#    it { is_expected.to have_login_shell '/bin/bash'}
end

describe service('tomcat_sigma_digital_fo') do
  it { should be_enabled   }
  it { should be_running   }
end 

describe port(8080) do
    it { is_expected.to be_listening }
end
