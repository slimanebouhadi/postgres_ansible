== Stime.postgres-role

Role d'installation d'instance postgresql dans le contexte des environnements ( uat, etude, preprod, prod ) serveur stime. +
Dans les environnements serveur stime, postgresql est le serveur de base de données par défaut, de ce fait son installation suit un certains nombres de "normes" stime.



== Requirements

Dans votre projet le fichier requirements.yml doit etre présent et contenir les éléments ci-dessous:
[source,yml]
----
---

 - src: git+http://git-stime.mousquetaires.com/cicd/stime.util-role.git
   name: stime.util-role
   version: master


 - src: git+http://git-stime.mousquetaires.com/cicd/stime.lvmfs-role.git
   name: stime.lvmfs-role
   version: master
----


== Dependencies

- [*] ansible: version  2.4.3
- [*] Jinja2: version 2.10
- [*] jmespath: version 0.9.3
- [*] lxml: verison 4.4.1

[source,shell]
----
pip install --proxy url_complete_du_proxy  le_nom_de_mon_module==la_version
----

== Role Variables

[width="80%",cols="1,2,3,4",options="header,footer"]
|====================================================================
|*variable*|*requis*|*valeur par default*|*détails*       
| postgres_instances  | oui | aucune | Variable de type "liste". Elle contient la liste des instances postgres à installer
| module_id   | oui   | aucune | Contient l'id du module applicatif concerné
| databases  | oui   | aucune | variable de type dictionnaire, Renseigne les le nom, le owner, le password et le schema de la base a creer 
| postgresql_pg_hba_custom |  oui  |  aucune  |  variable de type dictionnaire, renseigne les autorisation de connexion entrante à une base postgres
|====================================================================


Deux variables sont essentielles dans l'initialisation de l'instance postgres. + 
Le script ansible realisera un initdb en se basant sur la variable "databases" tel que ci-dessous

[source,yml]
----
---

databases:
    - { name: 'prhsxx01', owner: 'urhsxx01', pass: 'urhsxx01', schema: 'urhsxx01', state: 'present', encrypted: True }

----

La gestion de l'autorisation de connexion a l'instance postgresql est controlée par la variable "postgresql_pg_hba_custom" tel que decrit ci-dessous:

[source,yml]
----
---

postgresql_pg_hba_custom:
    - { type: host,  database: 'p{{ idgen }}01', user: 'u{{ idgen }}01', address: '10.122.203.0/24',  method: 'md5', comment: 'IPv4 remote connections:' }
   

----

Un formalisme ou une "abstraction" à été défini pour standardiser les playbooks de  déploiement devlab. Ce formalisme suit une structure hiérarchique que j'appelle la "Stack". Une Stack est composée d'un ou plusieurs  module(s) et chaque +
modules contient un ou plusieurs composants organisés selon la structure ci-dessous. Cette organisation ne peu convenir a tous les projets, mais fournis un template de projet ansible pour une majorité de projet devlab respectant  les "normes" stime. +

.Stack Structure
* *stack*
** *modules* 
*** name: module_name +
    *component_name* (ex: apache_instances,  mongo_instances, tomcat_instances ou postgres_instances ) +
**** name: instance_name +
     param-1: value +
     param-2: value +
     param-3: value +
     param-n: value +
     backends:
***** host: (definit le hostname ex: 1203pk001) +
      ajp_port

*** name: module_name +
    etc ...

Merci de vous reportez à l'extrait de playbook ci-dessous pour voir un exemple concret d'utilisation de la "stack"

== Example Playbook

[source,yaml]
----

hosts: front
  vars:
      appli: {'name': 'sigma-digital', 'code': 'xa', 'pays': 'fr', 'idx': '1'}
      env: {{ env }}
      env_prefix: "{{ env[0] }}"
      idgen: "{{ env_prefix }}{{ appli.code }}{{ appli.pays }}"
      user_svc: "{{ idgen }}{{ appli.idx }}"
      app_name: "{{ appli.name }}"
      managefs: False
      module_idx: 01
      module_ids: {'fo': 'sdfo', 'bo':'sdbo'}

      stack:
        modules:
        - name: sigma-fo
          postgres_instances:
              - name: "{{ appli.code }}"
                module_id: "{{ module_ids.fo }}"
                databases:
                      - { name: 'p{{ user_svc }}', owner: 'u{{ user_svc }}', pass: 'u{{ user_svc }}', schema: 'u{{ user_svc }}', state: 'present', encrypted: True }
                postgresql_pg_hba_custom:
                      - { type: host,  database: 'p{{ user_svc }}', user: 'u{{ user_svc }}', address: '10.122.203.0/24',  method: 'md5', comment: 'IPv4 remote connections:' }


  tasks:

  - name: Get the list of module
    set_fact:
       mods: '{{ stack.modules | list }}'
    tags: [always]

  - name: Get fact for postgres
    set_fact:
         postgres_instances: '{{ mods | selectattr("postgres_instances", "defined") | map(attribute="postgres_instances") | list }}'
    tags: [postgres,infra]

  - name: set facts databases
    set_fact: 
         databases: "{{ item.databases|default([]) }}"
    with_items: "{{ postgres_instances }}"
    loop_control:
         label: "{{ item.databases|default(omit) }}"
    tags: [postgres,infra]

  - name: set facts hba_custom
    set_fact: 
         postgres_hba_custom: "{{ item.postgresql_pg_hba_custom|default([]) }}"
    with_items: "{{ postgres_instances }}"
    loop_control:
         label: "{{ item.postgresql_pg_hba_custom|default(omit) }}"
    tags: [postgres,infra]

  - name: start <role> | call Postgres
    tags: ['postgres']
    include_role:
        name: stime.postgres-role
    
----



Author Information
------------------

[width="90%",options="header"]
|=======================================================
|Role                | Author infos | version document 
|stime.postgres-role | René Okouya rokouya@mousquetaires.com | 0.4-2018-03-26
|=======================================================